package app.login.master.login

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_status_login.*

class StatusLoginFragment : Fragment() {

    companion object {
        fun newInstance() = StatusLoginFragment()
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(
                R.layout.fragment_status_login,
                container,
                false
        )

    }

    fun receiveDataLogin(message: String) {
        status_txt.text = status_txt.text.toString() + "\n" + message
    }
}
