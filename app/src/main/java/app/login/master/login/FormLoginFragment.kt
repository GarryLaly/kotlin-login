package app.login.master.login

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_form_login.*

class FormLoginFragment : Fragment() {

    private lateinit var listener: LoginActionListener

    companion object {
        fun newInstance(listener: LoginActionListener): FormLoginFragment {
            val fragment = FormLoginFragment()
            fragment.listener = listener
            return fragment
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(
                R.layout.fragment_form_login,
                container,
                false
        )

    }

    interface LoginActionListener {
        fun onFormLoginReady(username: String, password: String)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btn_login.setOnClickListener {
            val username = username_txt.text.toString()
            val password = password_txt.text.toString()

            sendToOtherFragment(username, password)
        }
    }

    private fun sendToOtherFragment(username: String, password: String) {
        listener.onFormLoginReady(username, password)
    }
}
