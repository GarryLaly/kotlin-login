package app.login.master.login

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast

class MainActivity : AppCompatActivity(), FormLoginFragment.LoginActionListener {

    private lateinit var statusLoginFragment: StatusLoginFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tempelFragment1()
        tempelFragment2()
    }

    private fun tempelFragment1() {
        val fragment = FormLoginFragment.newInstance(this)

        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(
                R.id.fragment_container_1,
                fragment
        )
        fragmentTransaction.commit()
    }

    private fun tempelFragment2() {
        statusLoginFragment = StatusLoginFragment.newInstance()

        supportFragmentManager.beginTransaction().apply {
            replace(
                    R.id.fragment_container_2,
                    statusLoginFragment
            )
            commit()
        }
    }

    override fun onFormLoginReady(username: String, password: String) {
        var message = ""
        if (username.isEmpty() or password.isEmpty()) {
            message = "Username dan password tidak boleh kosong"
        }else if (!password.equals("stts")) {
            message = "Password harus `stts`"
        }else {
            message = "Login berhasil"

            sendToAnotherActivity(username)
        }

        Toast
                .makeText(
                        this,
                        message,
                        Toast.LENGTH_LONG
                )
                .show()

        statusLoginFragment.receiveDataLogin(message)
    }

    private fun sendToAnotherActivity(username: String) {
        val intent = DetailActivity.getStartIntent(
                this,
                username
        )
        startActivity(intent)
    }
}
